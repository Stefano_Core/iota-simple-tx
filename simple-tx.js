const IOTA = require('iota.lib.js');
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//Initialize variables
const nodeAddress = 'https://my-iota-node.com:14267'
const address = 'IOTA9ITALIA9MEETUP9MILANO99999999999999999999999999999999999999999999999999999999'
const minWeightMagnitude = 14
const myTxTag = 'IOTA9ITALIA9RULEZ'

// Create IOTA instance directly with provider
var iota = new IOTA({provider: nodeAddress});

//Send TX
sendTx = (myMessage) => {
  const message = iota.utils.toTrytes(myMessage)

  const transfers = [{
      address: address,
      value: 0,
      message: message,
      tag: myTxTag 
  }]
  
  console.log()
  console.log('Sending TX to Tangle...')
  iota.api.sendTransfer(address,3,minWeightMagnitude,transfers,(err, res) => {
  if (err){
    console.log('Problem with TX generation...')
    console.log(err)
  } else {
    console.log()
    console.log(res)
    console.log()
    console.log('Here the TX: https://thetangle.org/transaction/' + res[0].hash)
  }
  initilization()
})
}

initilization = () => {
  console.log('')
  console.log('---------------------------------------------------' + '\n')
  readline.question(`Type a message for your IOTA transaction: `, (text) => {
    //myMessage = text
    sendTx(text)
  })
}

//--------------------- START APPLICATION ---------------------
initilization()